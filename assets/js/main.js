$(function () {

    // OPEN CART
    $('#cart').click(function (e) {
        e.preventDefault();
        $('#main-cart').toggleClass('active');
        $('main').toggleClass('active-right')
    });

    $('#open-cart').click(function (e) {
        e.preventDefault();
        $('#main-cart').toggleClass('active');
        $('main').toggleClass('active-right')
        $('.header-sidebar__modal').addClass('active')
    });

    // CLOSE CART
    $('#close-cart').click(function (e) {
        e.preventDefault();
        $('#main-cart').removeClass('active');
        $('main').removeClass('active-right')
        $('.header-sidebar__modal').removeClass('active')

    });

    // CLICK ON MODAL AND CLOSE HEADER - CART
    $('.header-sidebar__modal').click(function (e) {
        e.preventDefault()
        $('#main-cart').removeClass('active');
        $('#header-sidebar').removeClass('active');
        $('main').removeClass('active-right')
        $('main').removeClass('active-left')
        $('#open-header').removeClass('active')
        $(this).removeClass('active')
    })

    // ADD TO CART CLICK OPEN CART SIDEBAR
    $('#add-to-cart').click(function (e) {
        e.preventDefault();
        $('#main-cart').addClass('active');
        $('main').addClass('active-right')
        $('.header-sidebar__modal').addClass('active')
    });

    // OPEN HEADER ON SMALL DISPLAY
    $('#open-header').click(function (e) {
        e.preventDefault();
        $('#header-sidebar').toggleClass('active');
        $('main').toggleClass('active-left');
        $(this).toggleClass('active');
        $('.header-sidebar__modal').toggleClass('active')
    });

    // SLIDE ON HOME PAGE
    $('.hero__slide').slick({
        nextArrow: $('#slick-next'),
        prevArrow: $('#slick-prev'),
        dots: true,
        appendDots: $('#dots-point'),
    });

    // SELECT PRODUCT TYPE IN PRODUCT-DETAILS
    $('.product-type__button button').click(function (e) {
        e.preventDefault();
        // console.log($(this).text());
        // console.log($('#product-type').val());
        $('#product-type').val($(this).text());
        $('.product-type__button button.active').removeClass('active');
        $(this).addClass('active');
    })

    // DE-INCREMENT CART
    let count = parseInt($('#product-count').val());
    let single_price = parseInt($('#cart-item-price').text());

    $('#decrement').click(function (e) {
        e.preventDefault();
        // console.log(typeof count);   
        count -= 1;
        single_price *= count;
        $('#product-count').val(count);
        $('#cart-item-price').text(single_price)
    });

    $('#increment').click(function (e) {
        e.preventDefault();
        // console.log(typeof count);   
        count += 1;
        console.log(single_price)
        single_price *= count;
        $('#product-count').val(count);
        $('#cart-item-price').text(single_price)
    });

    // CLICK CHECKBOX IN CHECKOUT   
    $('.form-label').click(function (e) {
        // console.log(123)
        $(this).find('.form-group__label__checkbox').toggleClass('active');
    });

    // SLIDE ON PRODUCT DETAILS
    $('.product__img__slide').slick({
        dots: true,
        arrows: true,
        prevArrow: $('#product-img-prev'),
        nextArrow: $('#product-img-next'),
        appendDots: $('.product__img__control ul')
    });
});